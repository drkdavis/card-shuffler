﻿using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

public class CardShuffler : MonoBehaviour
{
    public static CardShuffler Instance;

    public List<CardButton> PlayerCards = new List<CardButton>();

    [HideInInspector]
    public List<Card> MyCards = new List<Card>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    [PunRPC]
    public void DistributeCards(string deck)
    {
        List<Card> listOfCards = new List<Card>();

        for (var i = 0; i < 52; i++)
        {
            var card = new Card();

            card.NewCard(int.Parse(deck.Split(',')[i]));

            listOfCards.Add(card);
        }

        /*var bf = new BinaryFormatter();
        var listData = new MemoryStream(Convert.FromBase64String(deck));
        List<Card> cards = (List<Card>)bf.Deserialize(listData);*/

        var actorNumber = PhotonNetwork.LocalPlayer.ActorNumber - 1;

        for (var i = 0; i < 13; i++)
        {
            var customIndex = (actorNumber * 13) + i;

            MyCards.Add(listOfCards[customIndex]);
        }

        CreateDeck();
    }

    public void CreateDeck()
    {
        foreach(CardButton cb in PlayerCards)
        {
            cb.gameObject.SetActive(false);
        }

        for (var i = 0; i < MyCards.Count; i++)
        {
            PlayerCards[i].CreateCard(MyCards[i].CardNumber, i);
        }
    }

    public void UpdateDeck(List<Card> newCards)
    {
        for (var i = 0; i < newCards.Count; i++)
        {
            MyCards.Add(newCards[i]);
        }

        CreateDeck();
    }

    public void RemoveFromList(int index)
    {
        MyCards.RemoveAt(index);
    }
}