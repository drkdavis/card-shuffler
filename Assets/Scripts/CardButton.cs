﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardButton : MonoBehaviour
{
    public TextMeshProUGUI Card;
    public int CardValue;
    public CardSuit.Suit CardSuit;

    [HideInInspector]
    public int BtnIndex;

    private Button btn;

    private void Awake()
    {
        gameObject.SetActive(false);

        btn = GetComponent<Button>();
        btn.onClick.AddListener(CardSelected);
    }

    public void CreateCard(int value, int btnindex)//, CardSuit.Suit suit)
    {
        btn.interactable = true;

        gameObject.SetActive(true);
        CardValue = value;
        //CardSuit = suit;

        BtnIndex = btnindex;

        //Card.text = CardValue.ToString() + CardSuit.ToString();

        Card.text = CardValue.ToString();
    }

    private void CardSelected()
    {
        if (PhotonNetwork.IsMasterClient && MatchStats.Instance.isMastersTurn)
        {
            MatchStats.Instance.GetComponent<PhotonView>().RPC("PlayerPlayedACard", RpcTarget.All, CardValue);
            Debug.LogFormat("MASTER REMOVED CARD {0} from {1} POSITION", CardValue, BtnIndex);
            CardShuffler.Instance.RemoveFromList(BtnIndex);
            btn.interactable = false;
        }
        else if (!PhotonNetwork.IsMasterClient && !MatchStats.Instance.isMastersTurn)
        {
            MatchStats.Instance.GetComponent<PhotonView>().RPC("PlayerPlayedACard", RpcTarget.All, CardValue);
            CardShuffler.Instance.RemoveFromList(BtnIndex);
            btn.interactable = false;
        }
    }
}