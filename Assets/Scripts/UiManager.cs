﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    public static UiManager Instance;

    public List<TextMeshProUGUI> PlayersCardsTxt = new List<TextMeshProUGUI>();

    public TextMeshProUGUI PlayerName;

    public TextMeshProUGUI NextCardIndicator;

    public TextMeshProUGUI ActionLog;

    private const string YOURTURNMESSAGE = "ESCOLHA UMA CARTA", OPPONENTTURNMESSAGE = "ESPERANDO OPONENTE JOGAR...";

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        PlayerName.text = PhotonNetwork.LocalPlayer.NickName;

        LogMessage();
    }

    public void ShowPlayerCards(int playerIndex, Player playerCards)
    {
        PlayersCardsTxt[playerIndex].text = "PLAYER " + (playerIndex + 1).ToString() + " CARDS: " + playerCards.ToString();
    }

    public void IncreaseTurn(int turnValue)
    {
        NextCardIndicator.text = turnValue.ToString();
    }

    public void LogMessage()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            if (MatchStats.Instance.isMastersTurn)
            {
                ActionLog.text = YOURTURNMESSAGE;
            }
            else
            {
                ActionLog.text = OPPONENTTURNMESSAGE;
            }
        }
        else if (!PhotonNetwork.IsMasterClient)
        {
            if (MatchStats.Instance.isMastersTurn)
            {
                ActionLog.text = OPPONENTTURNMESSAGE;
            }
            else
            {
                ActionLog.text = YOURTURNMESSAGE;
            }
        }
    }
}