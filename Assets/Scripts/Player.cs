﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public List<Card> PlayerDeck = new List<Card>();

    public override string ToString()
    {
        return DeckToString();
    }

    public string DeckToString()
    {
        var cardsList = "";

        foreach (Card card in PlayerDeck)
        {
            cardsList += card + ", ";
        }

        return cardsList;
    }
}