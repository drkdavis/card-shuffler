﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MatchStats : MonoBehaviour
{
    public static MatchStats Instance;

    [HideInInspector]
    public int turnNumber = 1;

    private int lastCardPlayed;

    private List<Card> cardsPlayed = new List<Card>();

    [HideInInspector]
    public bool isMastersTurn = true;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    [PunRPC]
    public void PlayerPlayedACard(int cardValue)
    {
        lastCardPlayed = cardValue;

        var card = new Card();
        card.NewCard(cardValue);
        cardsPlayed.Add(card);

        isMastersTurn = !isMastersTurn;

        PassTurn();
    }

    /*[PunRPC]
    public void GuestPlayedACard(int cardValue)
    {
        lastCardPlayed = cardValue;

        var card = new Card();
        card.NewCard(cardValue);
        cardsPlayed.Add(card);

        isMastersTurn = !isMastersTurn;

        PassTurn();
    }*/

    private void PassTurn()
    {
        UiManager.Instance.LogMessage();
        turnNumber++;

        if (turnNumber > 13)
        {
            turnNumber = 1;
        }

        UiManager.Instance.IncreaseTurn(turnNumber);
    }

    public void BluffCalled()
    {
        if (turnNumber == 1)
            return;

        Debug.Log("LAST CARD PLAYED " + lastCardPlayed.ToString() + " (ON TURN " + (turnNumber - 1).ToString() + ")");

        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("MASTER CALLED BLUFF...");
            if (turnNumber - 1 != lastCardPlayed)
            {
                Debug.Log("...AND WON");
                MasterWonBluff();
            }
            else
            {
                Debug.Log("...AND LOSE");
                GuestWonBluff();
            }
        }
        else
        {
            Debug.Log("GUEST CALLED BLUFF...");
            if (turnNumber - 1 != lastCardPlayed)
            {
                Debug.Log("...AND WON");
                GuestWonBluff();
            }
            else
            {
                Debug.Log("...AND LOSE");
                MasterWonBluff();
            }
        }
    }

    private void MasterWonBluff()
    {
        GetComponent<PhotonView>().RPC("UpdatePlayerDeck", RpcTarget.All, 1);
    }

    private void GuestWonBluff()
    {
        GetComponent<PhotonView>().RPC("UpdatePlayerDeck", RpcTarget.All, 2);
    }

    [PunRPC]
    public void UpdatePlayerDeck(int playerId)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == playerId)
        {
            CardShuffler.Instance.UpdateDeck(cardsPlayed);
        }
        else
        {
            CardShuffler.Instance.CreateDeck();
        }

        cardsPlayed.Clear();
    }
}