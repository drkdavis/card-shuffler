﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DeckCreator : MonoBehaviour
{
    public GameObject MatchHandler;
    public List<Card> DeckOfCards = new List<Card>();

    public List<int> CardValues = new List<int>();

    private void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        for (var i = 0; i < 4; i++) // i = naipes
        {            
            for (var l = 1; l < 14; l++) // l = números/cartas do baralho
            {
                /*var card = new Card();
                card.NewCard(l);

                DeckOfCards.Add(card);*/

                CardValues.Add(l);
            }
        }

        //RandomizeDeck(DeckOfCards);

        RandomizeDeck(CardValues);

        var cardList = "";

        foreach(int card in CardValues)
        {
            cardList += card.ToString() + ",";
        }

        //GetComponent<CardShuffler>().DistributeCards(DeckOfCards);

        /*var ms = new MemoryStream();
        var bf = new BinaryFormatter();

        bf.Serialize(ms, DeckOfCards);

        var data = Convert.ToBase64String(ms.GetBuffer());*/

        GetComponent<PhotonView>().RPC("DistributeCards", RpcTarget.All, cardList);
    }

    private void RandomizeDeck(List<int> deck)
    {
        for (int i = 0; i < deck.Count; i++)
        {
            int temp = deck[i];
            int randomIndex = UnityEngine.Random.Range(i, deck.Count);
            deck[i] = deck[randomIndex];
            deck[randomIndex] = temp;
        }
    }
}