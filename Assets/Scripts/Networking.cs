﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Networking : MonoBehaviourPunCallbacks
{
    private string _roomName = "GameRoom";

    public TextMeshProUGUI PlayerCountTxt;
    public TMP_InputField PlayerName;

    public GameObject Options;

    public Button CreateGameBtn, JoinGameBtn, PlayGameBtn;

    private bool roomIsOn;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        Options.SetActive(false);
        VerifyPlayerName();
    }

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    #region Pun Callbacks

    public override void OnConnectedToMaster()
    {
        Options.SetActive(true);
    }

    #endregion

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(_roomName);
        PhotonNetwork.NickName = PlayerName.text;
    }

    public override void OnCreatedRoom()
    {
        roomIsOn = true;
        PlayGameBtn.gameObject.SetActive(true);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
    }

    public void JoinRoom()
    {
        PhotonNetwork.NickName = PlayerName.text;
        Debug.Log("Trying to Join room...");
        PhotonNetwork.JoinRoom(_roomName);
    }

    public void VerifyPlayerName()
    {
        CreateGameBtn.interactable = !string.IsNullOrEmpty(PlayerName.text);
        JoinGameBtn.interactable = !string.IsNullOrEmpty(PlayerName.text);
        PlayGameBtn.interactable = !string.IsNullOrEmpty(PlayerName.text);
    }

    private void Update()
    {
        if (roomIsOn)
            PlayerCountTxt.text = PhotonNetwork.CurrentRoom.PlayerCount.ToString();
    }

    // -------------------------------------------------------------------------------------------------------

    public void CloseGame()
    {
        Application.Quit();
    }

    public void PlayGame()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            SceneManager.LoadScene(1);
        }
    }
}