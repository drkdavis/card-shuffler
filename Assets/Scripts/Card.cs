﻿using System;
using UnityEngine;

[Serializable]
public class Card : MonoBehaviour
{
    public int CardNumber;
    //public CardSuit.Suit CardSuit;
    public string CardSuit;
    public string CardCode;

    public void NewCard (int number)//, string suitIndex)
    {
        CardNumber = number;
        //CardSuit = (CardSuit.Suit)suitIndex;
        CardCode = CardNumber.ToString();
    }

    public override string ToString()
    {
        return CardCode;
    }
}